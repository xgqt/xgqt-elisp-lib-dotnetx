;;; elisp-common.el --- Shared ELisp code for repository maintenance -*- lexical-binding: t -*-


;; This file is part of xgqt-elisp-lib-dotnetx - .NET extras for GNU Emacs.
;; Copyright (c) 2023-2024, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License

;; xgqt-elisp-lib-dotnetx is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; xgqt-elisp-lib-dotnetx is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with xgqt-elisp-lib-dotnetx.  If not, see <https://www.gnu.org/licenses/>.



;;; Commentary:


;; Shared ELisp code for repository maintenance.



;;; Code:


(require 'package)


(defun time ()
  "Return current time as a string."
  (format-time-string "%H:%M:%S"))

(defun log-message (message-type &rest format-args)
  "Log a message of type MESSAGE-TYPE constructed from FORMAT-ARGS."
  (message " [%s %s] %s" (time) message-type (apply #'format format-args)))

(defun install-elisp-module (module-directory)
  "Install a ELisp module inside the MODULE-DIRECTORY."
  (let ((module-name
         (file-name-base module-directory)))
    (log-message "DBG" "Installing module %s" module-name)
    (package-install-file module-directory)
    (log-message "OK " "Successfully installed %s" module-name)))


(provide 'elisp-common)



;;; elisp-common.el ends here
