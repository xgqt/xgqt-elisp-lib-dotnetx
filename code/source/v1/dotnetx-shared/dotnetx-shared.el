;;; dotnetx-shared.el --- Shared code among dotnetx-* packages -*- lexical-binding: t -*-


;; This file is part of xgqt-elisp-lib-dotnetx - .NET extras for GNU Emacs.
;; Copyright (c) 2023-2024, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License

;; xgqt-elisp-lib-dotnetx is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; xgqt-elisp-lib-dotnetx is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with xgqt-elisp-lib-dotnetx.  If not, see <https://www.gnu.org/licenses/>.


;; Authors: Maciej Barć <xgqt@riseup.net>
;; Created: 30.12.2023
;; Version: 1.1.0
;; Keywords: convenience
;; Homepage: https://gitlab.com/xgqt/xgqt-elisp-lib-dotnetx/
;; Package-Requires: ((emacs "27.1") (dash "2.19.0") (s "1.12.0"))
;; SPDX-License-Identifier: GPL-2.0-or-later



;;; Commentary:


;; Shared code among dotnetx-* packages for GNU Emacs.



;;; Code:


(require 'dash)
(require 's)


(defconst dotnetx-shared-tools
  (expand-file-name "~/.dotnet/tools"))


(defvar dotnetx-shared-tool-csrepl nil
  "Path to csharprepl.")

(defvar dotnetx-shared-tool-pwsh nil
  "Path to PowerShell.")


(defun dotnetx-shared--create-command (command command-args)
  "Create a command string from COMMAND and COMMAND-ARGS."
  (->> command-args
       (s-join " ")
       (format "%s %s" command)))

(defun dotnetx-shared--install-tool (tool-name)
  "Install a .NET tool named TOOL-NAME."
  (->> tool-name
       (format "dotnet tool install --global %s")
       shell-command))

(defun dotnetx-shared--expand-tool (tool-name)
  "Expand the path to the TOOL-NAME."
  (expand-file-name tool-name dotnetx-shared-tools))

(defun dotnetx-shared--prepare-environment ()
  "Prepare the system environment variables before running .NET commands."
  (let ((env-var-map
         '(("DOTNET_CLI_TELEMETRY_OPTOUT" . "1")
           ("DOTNET_NOLOGO" . "1")
           ("DOTNET_SKIP_FIRST_TIME_EXPERIENCE" . "1")
           ("POWERSHELL_TELEMETRY_OPTOUT" . "1")
           ("POWERSHELL_UPDATECHECK" . "0"))))
    (mapc (lambda (env-var-pair)
            (setenv (car env-var-pair) (cdr env-var-pair)))
          env-var-map)))


(defmacro dotnetx-shared--setup-command (command-name command-path-var)
  "Setup command path variable COMMAND-PATH-VAR for command COMMAND-NAME."
  `(let ()
     (cond
      (,command-path-var
       nil)
      (t
       (let ((exe-path
              (executable-find ,command-name)))
         (cond
          (exe-path
           (setq ,command-path-var exe-path))
          (t
           (dotnetx-shared--install-tool ,command-name)
           (setq ,command-path-var
                 (dotnetx-shared--expand-tool ,command-name)))))))))


(provide 'dotnetx-shared)



;;; dotnetx-shared.el ends here
