;;; dotnetx-eat.el --- Useful EAT commands for .NET -*- lexical-binding: t -*-


;; This file is part of xgqt-elisp-lib-dotnetx - .NET extras for GNU Emacs.
;; Copyright (c) 2023-2024, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License

;; xgqt-elisp-lib-dotnetx is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; xgqt-elisp-lib-dotnetx is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with xgqt-elisp-lib-dotnetx.  If not, see <https://www.gnu.org/licenses/>.


;; Authors: Maciej Barć <xgqt@riseup.net>
;; Created: 30.12.2023
;; Version: 1.1.0
;; Keywords: convenience
;; Homepage: https://gitlab.com/xgqt/xgqt-elisp-lib-dotnetx/
;; Package-Requires: ((emacs "27.1") (eat "0.9.4") (dotnetx-shared "1.1.0"))
;; SPDX-License-Identifier: GPL-2.0-or-later



;;; Commentary:


;; Useful Emacs EAT commands for running .NET tools inside GNU Emacs.



;;; Code:


(require 'eat)

(require 'dash)
(require 's)

(require 'dotnetx-shared)


;;;###autoload
(defun eat-csrepl ()
  "EAT the C# interactive REPL."
  (interactive)

  (dotnetx-shared--prepare-environment)
  (dotnetx-shared--setup-command "csharprepl" dotnetx-shared-tool-csrepl)

  (let ((command-args
         `("--useTerminalPaletteTheme")))
    (->> command-args
         (dotnetx-shared--create-command dotnetx-shared-tool-csrepl)
         (eat))))

;;;###autoload
(defun eat-fsi ()
  "EAT the F# interactive REPL."
  (interactive)

  (dotnetx-shared--prepare-environment)

  (let ((command-args
         `("--nologo")))
    (->> command-args
         (dotnetx-shared--create-command "dotnet fsi")
         (eat))))

;;;###autoload
(defun eat-pwsh ()
  "EAT the PowerShell REPL."
  (interactive)

  (dotnetx-shared--prepare-environment)
  (dotnetx-shared--setup-command "pwsh" dotnetx-shared-tool-pwsh)

  (let ((command-args
         `("-Interactive"
           "-Login"
           "-NoLogo"
           "-NoProfileLoadTime"
           ,(format "-WorkingDirectory %s" default-directory))))
    (->> command-args
         (dotnetx-shared--create-command dotnetx-shared-tool-pwsh)
         (eat))))


(provide 'dotnetx-eat)



;;; dotnetx-eat.el ends here
