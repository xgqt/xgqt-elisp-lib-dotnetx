;;; dotnetx-vterm.el --- Useful VTerm commands for .NET -*- lexical-binding: t -*-


;; This file is part of xgqt-elisp-lib-dotnetx - .NET extras for GNU Emacs.
;; Copyright (c) 2023-2024, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License

;; xgqt-elisp-lib-dotnetx is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; xgqt-elisp-lib-dotnetx is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with xgqt-elisp-lib-dotnetx.  If not, see <https://www.gnu.org/licenses/>.


;; Authors: Maciej Barć <xgqt@riseup.net>
;; Created: 30.12.2023
;; Version: 1.1.0
;; Keywords: convenience
;; Homepage: https://gitlab.com/xgqt/xgqt-elisp-lib-dotnetx/
;; Package-Requires: ((emacs "27.1") (vterm "0.0.2") (dotnetx-shared "1.1.0"))
;; SPDX-License-Identifier: GPL-2.0-or-later



;;; Commentary:


;; Useful Emacs VTerm commands for running .NET tools inside GNU Emacs.



;;; Code:


(require 'vterm)

(require 'dash)
(require 's)

(require 'dotnetx-shared)


;;;###autoload
(defun vterm-csrepl ()
  "VTerm support for the C# interactive REPL."
  (interactive)

  (dotnetx-shared--prepare-environment)
  (dotnetx-shared--setup-command "csharprepl" dotnetx-shared-tool-csrepl)

  (let* ((command-args
          `("--useTerminalPaletteTheme"))
         (vterm-shell
          (dotnetx-shared--create-command dotnetx-shared-tool-csrepl command-args)))
    (vterm current-prefix-arg)))

;;;###autoload
(defun vterm-fsi ()
  "VTerm support for the F# interactive REPL."
  (interactive)

  (dotnetx-shared--prepare-environment)

  (let* ((command-args
          `("--nologo"))
         (vterm-shell
          (dotnetx-shared--create-command "dotnet fsi" command-args)))
    (vterm current-prefix-arg)))

;;;###autoload
(defun vterm-pwsh ()
  "VTerm support for the PowerShell REPL."
  (interactive)

  (dotnetx-shared--prepare-environment)
  (dotnetx-shared--setup-command "pwsh" dotnetx-shared-tool-pwsh)

  (let* ((command-args
          `("-Interactive"
            "-Login"
            "-NoLogo"
            "-NoProfileLoadTime"
            ,(format "-WorkingDirectory %s" default-directory)))
         (vterm-shell
          (dotnetx-shared--create-command dotnetx-shared-tool-pwsh command-args)))
    (vterm)))


(provide 'dotnetx-vterm)



;;; dotnetx-vterm.el ends here
