:;exec emacs --no-site-file -batch -q -l "${0}" -f main # -*- Emacs-Lisp -*-


;; This file is part of xgqt-elisp-lib-dotnetx - .NET extras for GNU Emacs.
;; Copyright (c) 2023-2024, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License

;; xgqt-elisp-lib-dotnetx is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; xgqt-elisp-lib-dotnetx is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with xgqt-elisp-lib-dotnetx.  If not, see <https://www.gnu.org/licenses/>.



;;; Commentary:


;; Install all ELisp subpackages of dotnetx.



;;; Code:


(require 'package)

(require 'elisp-common
         (expand-file-name "../elisp/elisp-common.el"
                           (file-name-directory load-file-name)))


(defconst admin-directory
  (expand-file-name (file-name-directory load-file-name)))

(defconst source-directory
  (expand-file-name "../" admin-directory))

;; NOTE: Need to list the modules statically for proper installation order.
(defconst dotnetx-module-names
  '("dotnetx-shared"
    "dotnetx-eat"
    "dotnetx-vterm"))

(defconst dotnetx-module-paths
  (mapcar (lambda (path)
            (expand-file-name path source-directory))
          dotnetx-module-names))


(defun main ()
  "Main."
  (log-message "DBG" "Admin directory is %s" admin-directory)
  (log-message "DBG" "Source directory is %s" source-directory)
  (setq package-archives
        '(("gnu"    . "https://elpa.gnu.org/packages/")
          ("melpa"  . "https://melpa.org/packages/")
          ("nongnu" . "https://elpa.nongnu.org/nongnu/")
          ("xgqt"   . "https://xgqt.gitlab.io/emacs-gitlab-elpa/packages/")))
  (log-message "DBG" "Refreshing ELisp package archives")
  (package-refresh-contents)
  (mapc #'install-elisp-module dotnetx-module-paths)
  (log-message "OK " "All subpackage modules installed successfully"))



;;; emacs_install_all.el ends here
