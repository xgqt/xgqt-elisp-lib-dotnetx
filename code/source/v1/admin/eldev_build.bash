#!/usr/bin/env bash

# This file is part of xgqt-elisp-lib-dotnetx - .NET extras for GNU Emacs.
# Copyright (c) 2023-2024, Maciej Barć <xgqt@riseup.net>
# Licensed under the GNU GPL v2 License

# xgqt-elisp-lib-dotnetx is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# xgqt-elisp-lib-dotnetx is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with xgqt-elisp-lib-dotnetx.  If not, see <https://www.gnu.org/licenses/>.

trap 'exit 128' INT
set -e

cd "${1:-.}"

while read -r submodule ; do
    submodule_name="$(basename "${submodule}")"

    echo " [$(date '+%H:%M:%S') DBG] Building subpackage module ${submodule_name}"
    cd "${submodule}"

    eldev build --force-all

    echo " [$(date '+%H:%M:%S') OK ] Successfully built subpackage module ${submodule_name}"
done < <(find "$(pwd)" -maxdepth 1 -type d -name "dotnetx-*")
