;; -*- no-byte-compile: t -*-

;; Directory Local Variables
;; For more information see (info "(emacs) Directory Variables")

((find-file . ((indent-tabs-mode . nil)
               (require-final-newline . t)
               (show-trailing-whitespace . t)))
 (makefile-mode . ((indent-tabs-mode . t))))
