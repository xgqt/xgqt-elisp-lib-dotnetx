# ELisp .NET-X

.NET extras for GNU Emacs.

## About

This repository provides ELisp modules that add extra functionality for
interacting with the .NET ecosystem inside GNU Emacs.

## License

### Code

Code in this project is licensed under the MPL, version 2.0.

    This file is part of xgqt-elisp-lib-dotnetx - .NET extras for GNU Emacs.
    Copyright (c) 2023-2024, Maciej Barć <xgqt@riseup.net>
    Licensed under the GNU GPL v2 License
